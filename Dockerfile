FROM registry.gitlab.com/eyeo/docker/abpchromium_gitlab-runner:202203.1

# build_and_test_desktop_release - before_script
RUN apt-get update && apt-get install -y lsof
RUN pip3 install python-gitlab

# use tfeliu user
RUN useradd -ms /bin/bash tfeliu
RUN usermod -aG sudo tfeliu
RUN echo "Defaults:tfeliu !requiretty" >> /etc/sudoers
RUN chown -R tfeliu:tfeliu /opt/ ~

RUN mkdir chromium-sdk && chown -R tfeliu:tfeliu chromium-sdk
# clone project
RUN runuser -p -u tfeliu -- git clone https://gitlab.com/eyeo/adblockplus/chromium-sdk.git chromium-sdk/src
WORKDIR chromium-sdk/src

# common_build_chromium_before - docker build part
RUN ./.ci-scripts/install_packages.sh

# gclient sync
ARG PATH=$PATH:$DEPOT_TOOLS
RUN cp gclient/.gclient_ci_linux ../.gclient
RUN cd .. && runuser -p -u tfeliu -- gclient sync --force --reset --delete_unversioned_trees | grep -v '=='

EXPOSE 8088
ENV DEPOT_TOOLS $DEPOT_TOOLS
COPY entrypoint*.sh .
ENTRYPOINT ./entrypoint-build.sh

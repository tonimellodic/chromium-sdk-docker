#! /bin/bash -x

export PATH=$PATH:${DEPOT_TOOLS}:/chromium-sdk/src/third_party/llvm-build/Release+Asserts/bin

# adblocking logic sanity check
./out/Release/verify_flatbuffer_adblocking

# TODO: Vanilla not working
# Reason: https://stackoverflow.com/questions/66284256/gitlab-release-links-api-404-project-not-found
VERSION=$(./out/Release/chrome --product-version)
DOWNLOAD_DIR="test_reports"
# python3 .ci-scripts/get_vanilla_job_test_report.py ${VERSION} ${DOWNLOAD_DIR}

runuser -p -u tfeliu -- xvfb-run ./out/Release/bin/run_unit_tests --gtest_output="xml:out/Release/unit_tests_report.xml"
runuser -p -u tfeliu -- xvfb-run ./out/Release/bin/run_components_unittests --gtest_output="xml:out/Release/components_unittests_report.xml"

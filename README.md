# chromium-sdk-docker

Run chromium-sdk in Docker.

## Getting started

Before running this, replace `tfeliu` user in all files with your own local
user.

```sh
# Using `time` before docker commands is optional.

# Estimated build time (MacOS): git clone chromium-sdk - 4145s,
#   gclient sync - 3352s, total time - 7880s (131 min)
time docker build -t chromium-sdk .

# Estimated run time: 38:43.62 total (min)
time docker run -it --volume "/Users/tfeliu/.goma_client_oauth2_config:/home/tfeliu/.goma_client_oauth2_config" -p 127.0.0.1:8088:8088 chromium-sdk

# Run tests
docker commit $(docker ps -aqf ancestor=chromium-sdk | head -n 1) chromium-sdk-tests
# Estimated run time: ?
time docker run -it --entrypoint "./entrypoint-tests.sh" chromium-sdk-tests

# Copy test results
docker cp $(docker ps -aqf ancestor=chromium-sdk-tests | head -n 1):out/Release/unit_tests_report.xml .
docker cp $(docker ps -aqf ancestor=chromium-sdk-tests | head -n 1):out/Release/components_unittests_report.xml .
```

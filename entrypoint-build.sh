#! /bin/bash -x

set -eu

export PATH=$PATH:${DEPOT_TOOLS}:/chromium-sdk/src/third_party/llvm-build/Release+Asserts/bin
export GOMA_OAUTH2_CONFIG_FILE=/home/tfeliu/.goma_client_oauth2_config
export GOMA_SERVER_HOST=sunstone.goma.engflow.com
export GOMA_SERVER_PORT=443
export GOMACTL_USE_PROXY="false"
export GOMA_USE_LOCAL="false"
export NUMJOBS=150

# common_build_chromium_before - docker run part
chown -R tfeliu:tfeliu /home/tfeliu/.goma_client_oauth2_config
runuser -p -u tfeliu -- goma_ctl ensure_start

# script
runuser -p -u tfeliu -- gn gen --check --args='disable_fieldtrial_testing_config=true is_debug=false use_goma=true enable_nacl=false  proprietary_codecs=true ffmpeg_branding="Chrome"' out/Release
runuser -p -u tfeliu -- ninja -j${NUMJOBS} -C out/Release chrome unit_tests components_unittests browser_tests verify_flatbuffer_adblocking
runuser -p -u tfeliu -- goma_ctl stat
